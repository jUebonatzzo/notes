# Kali Configurations

## Create user with sudo rights
```bash
useradd -m username
passwd username
usermod -a -G sudo username
chsh -s /bin/bash username
```


## Tmux Config

install [tmux-logging](https://github.com/tmux-plugins/tmux-logging)

modify `/root/.tmux.conf`. Extracted from [ippsec](https://www.youtube.com/watch?v=Lqehvpe_djs).


## Kali instalations

Add [FoxyProxy](moz-extension://202ac95b-9fa5-4011-80d8-c4aa3be575f7/first-install.html) to firefox

Install [gobuster](https://github.com/OJ/gobuster)

Install burpsuite CA

Install [xss-validator](https://github.com/nVisium/xssValidator)


## Firefox config

First search for: `about:config` and then modify these:

1. browser.urlbar.filter.javascript=“false”
1. network.captive-portal-service.enabled=“false”
1. security.tls.version.max=3



